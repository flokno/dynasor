# -*- coding: utf-8 -*-

"""
dynasor module.
"""

__project__ = 'dynasor'
__description__ = 'A tool for calculating dynamical structure factors'
__authors__ = ['Mattias Slabanja',
               'Erik Fransson']
__copyright__ = '2018'
__license__ = 'GPL2+'
__credits__ = ['The dynasor developers team']
__version__ = '0.1.2'
__maintainer__ = 'The dynasor developers team'
__maintainer_email__ = 'dynasor@materialsmodeling.org'
__status__ = 'beta-version'
__url__ = 'http://dynasor.materialsmodeling.org/'
