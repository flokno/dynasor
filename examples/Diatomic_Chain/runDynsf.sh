#!/bin/sh

#
# Options
K_BINS=21
TIME_WINDOW=500
MAX_FRAMES=10000
K_POINTS=21

dt=50 # time difference between to frames in the trajectory file 

k=31.4159265359 # 2pi/a in nanometer^-1

TRAJECTORY="lammpsrun/dumpT10.gz"
OUTPUT="outputs/dynasor_1Dchain"
INDEXFILE="index.1000"

dynasor -f "$TRAJECTORY" \
	--index=$INDEXFILE \
	--om=$OUTPUT.m \
	--op=$OUTPUT.pickle \
    --k-bins=$K_BINS \
	--max-frames=$MAX_FRAMES \
	--nt=$TIME_WINDOW \
	--dt=$dt \
	--k-sampling="line" \
	--k-points=$K_POINTS \
	--k-direction=$k,0,0


























